OC.L10N.register(
    "files_fulltextsearch",
    {
    "Go to folder" : "Gå til mappe",
    "Name" : "Navn",
    "Modified" : "Endret",
    "Size" : "Størrelse",
    "Copied {origin} inside {destination}" : "Kopierte {origin} i {destination}",
    "Files" : "Filer"
},
"nplurals=2; plural=(n != 1);");
