# Changelog

### 0.99.2 

- fix identification of source of files.


### 0.99.1 Release Candidate 2

- bugfix: option within_dir
- improvement: tags/metatags/subtags
- improvement: document and searchrequest are dispatched via event
- improvement: info update during :index
- improvement: no more chunks
- tesseract is removed from files_fts 


### 0.99.0 Release Candidate


### 0.7.0

- bugfixes
- changes in settings does not require a full reindex anymore
- compat with strange userId in shared name
- limit search within current directory
- filters by extension
- improved index and search within local files, external filesystem and group folders
- compat fulltextsearch 0.7.0 and fulltextsearch navigation app.



### 0.6.0

- Options panel
- bugfixes



### 0.5.0

- more options
- fixing some office mimetype detection



### v0.4.0

- fullnextsearch -> fulltextsearch
- settings panel


### v0.3.1

- bugfixes.



### BETA v0.3.0

- First Beta

